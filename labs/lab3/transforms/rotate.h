#ifndef ROTATE_H
#define ROTATE_H

#include "../structs/matrix.h"

Matrix create_rotation_matrix(const float);
std::vector<float> rotate(const Matrix, const float);

#endif
