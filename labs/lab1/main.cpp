/*
 * Author:          Dana Parker
 * Collaborator(s): Troy Oster
 * 
 * Links that helped me along the way:
 *     http://totologic.blogspot.com/2014/01/accurate-point-in-triangle-test.html
 */

#include <iostream>
#include "bitmap_image.hpp"

using namespace std;

// Macros for user-inputted point vector indicies
#define X_CORD 0
#define Y_CORD 1
#define RED    2
#define GREEN  3
#define BLUE   4

// Macros for bounding box vector indicies
#define BB_X_MAX    0
#define BB_Y_MAX    1
#define BB_X_MIN    2
#define BB_Y_MIN    3

// Macros for image resolution
#define IMAGE_WIDTH    640
#define IMAGE_HEIGHT   480

// Gets 3 points (x, y, r, g, b) seperated by whitespace
vector<float>
get_user_input() {
    float x;
    float y;
    float r;
    float g;
    float b;
    vector<float> vec;

    if (cin >> x >> y >> r >> g >> b) {

        vec.push_back(x);
        vec.push_back(y);
        vec.push_back(r);
        vec.push_back(g);
        vec.push_back(b);

        return vec;
    }

    else {
        cin.clear();
        cin.ignore();
        cout << "Error: Invalid input. Re-run program to try again." << endl;
        exit(0);
    }
}

void
print_vector(vector<float> const &vec) {
    for(int i = 0; i < vec.size(); i++) {
        cout << vec.at(i) << ' ';
    }
    cout << "" << endl;
}

// Find the bounding box of a triangle.
//
// Returns a vector containing
// a max x-value and a max y-value
// of the input triangle.
vector<float>
calc_bounding_box(vector<float> const (&triangle)[3]) {
    vector<float> box;
    float x1 = triangle[0].at(X_CORD);
    float y1 = triangle[0].at(Y_CORD);
    float x2 = triangle[1].at(X_CORD);
    float y2 = triangle[1].at(Y_CORD);
    float x3 = triangle[2].at(X_CORD);
    float y3 = triangle[2].at(Y_CORD);

    box.push_back(max(x1, max(x2, x3))); // max x-value
    box.push_back(max(y1, max(y2, y3))); // max y-value
    box.push_back(min(x1, min(x2, x3))); // min x-value
    box.push_back(min(y1, min(y2, y3))); // min y-value

    return box;
}

void
draw_triangle(
    vector<float> points[],
    vector<float> bbox,
    bitmap_image& image) {

    float red;
    float green;
    float blue;

    rgb_t color;

    const float x1 = points[0].at(X_CORD);
    const float y1 = points[0].at(Y_CORD);
    const float x2 = points[1].at(X_CORD);
    const float y2 = points[1].at(Y_CORD);
    const float x3 = points[2].at(X_CORD);
    const float y3 = points[2].at(Y_CORD);
    
    const float r1 = points[0].at(RED)   * 255;
    const float g1 = points[0].at(GREEN) * 255;
    const float b1 = points[0].at(BLUE)  * 255;
    const float r2 = points[1].at(RED)   * 255;
    const float g2 = points[1].at(GREEN) * 255;
    const float b2 = points[1].at(BLUE)  * 255;
    const float r3 = points[2].at(RED)   * 255;
    const float g3 = points[2].at(GREEN) * 255;
    const float b3 = points[2].at(BLUE)  * 255;

    float A;
    float B;
    float C;
    
    for(int x = bbox.at(BB_X_MIN); x < bbox.at(BB_X_MAX); x++) {
        for(int y = bbox.at(BB_Y_MIN); y < bbox.at(BB_Y_MAX); y++) {

            A = ( (y2 - y3) * (Px - x3) + (x3 - x2) * (Py - y3) )
              / ( (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3) );

            B = ( (y3 - y1) * (Px - x3) + (x1 - x3) * (Py - y3) )
              / ( (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3) );

            C = 1 - A - B;

            red = (r1 * A) + (r2 * B) + (r3 * C);
            green = (g1 * A) + (g2 * B) + (g3 * C);
            blue = (b1 * A) + (b2 * B) + (b3 * C);
            color = make_colour(red, green, blue);

            if(A >= 0 && A <= 1
               && B >= 0 && B <= 1
               && C >= 0 && C <= 1) {
                   image.set_pixel(Px, Py, color);
               }
        }
    }
}

int
main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.
    */

    cout << "\nEnter 3 points (enter a point as x y r g b):" << endl;
    vector<float> points[3];

    for (int i = 0;
         i < sizeof(points) / sizeof(points[0]);
         i++) {
             if (i != 0) {
                 cout << "\nEnter x y r g b of point " << i+1 << ":" << endl;
             }

             points[i] = get_user_input();
             cout << "\nContents of point " << i+1 << ":" << endl;
             print_vector(points[i]);
         }

    /*
      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(IMAGE_WIDTH, IMAGE_HEIGHT);

    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);
    */

    vector<float> bounding_box = calc_bounding_box(points);

    /*
      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    draw_triangle(points, bounding_box, image);

    image.save_image("triangle.bmp");
    cout << "Success" << endl;
}
