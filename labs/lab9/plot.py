import matplotlib.pyplot as plt
plt.style.use('ggplot')

# Create data
render_time = [4341.62, 8401.57, 12173.9, 16323.6, 20529.9,
               24717.5, 28735.0, 32748.9, 36661.6, 40012.2,
               43803.8, 48901.6, 51597.1, 55348.5, 59636.7,
               64708.6]
x_pos = [i for i, _ in enumerate(range(len(render_time)))]
labels = [str(i + 1) for i in range(len(render_time))]

# Create bar graph
plt.bar(x_pos, render_time, color='green')
plt.xlabel("Number of Boxes")
plt.ylabel("Render time in milliseconds")
plt.title("Render time of different image settings")

plt.xticks(x_pos, labels)

# Render the bar graph
plt.show()
