#include <iostream>
#include <glm/glm.hpp>
#include <bitmap/bitmap_image.hpp>

#include <cmath>

#define PERSPECTIVE_MODE   0
#define ORTHOGRAPHGIC_MODE 1

#define BG_R 20
#define BG_G 20
#define BG_B 20

#define IMAGE_WIDTH  640
#define IMAGE_HEIGHT 480

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;

    Ray(const glm::vec3& origin = glm::vec3(0, 0, 0),
        const glm::vec3& direction = glm::vec3(0, 0, 1))
    : origin(origin), direction(glm::normalize(direction))
    {}
};

// Inspired by
// http://viclw17.github.io/2018/07/16/raytracing-ray-sphere-intersection/
float intersection(const Ray& ray, const Sphere& sphere) {
    glm::vec3 o = ray.origin;
    glm::vec3 d = ray.direction;
    glm::vec3 ce = sphere.center;
    float r = sphere.radius;

    float a = glm::dot(d, d);
    float b = 2.0 * glm::dot(o-ce, d);
    float c = glm::dot(o-ce, o-ce) - r*r;

    float discriminant = b*b - 4*a*c;
    if (discriminant < 0) {
        return FLT_MAX;
    }

    float numerator = -b - sqrt(discriminant);
    if (numerator > 0)
        return numerator / (2.0 * a);

    numerator = -b + sqrt(discriminant);
    if (numerator > 0)
        return numerator / (2.0 * a);

    return FLT_MAX;
}

void render(int mode, bitmap_image& image, const std::vector<Sphere>& world) {
    float r = 5;
    float l = -5;
    float t = -5;
    float b = 5;

    // Iterate through every pixel
    for (int i = 0; i < IMAGE_WIDTH; i++) {
        for (int j = 0; j < IMAGE_HEIGHT; j++) {

            // Create ray for current pixel
            float ui = l  + (r-l) * (i + 0.5) / IMAGE_WIDTH;
            float vj = b  + (t-b) * (j + 0.5) / IMAGE_HEIGHT;

            Ray ray;
            if (mode == ORTHOGRAPHGIC_MODE) {
                ray.origin = glm::vec3(ui, vj, 0);
                ray.direction = glm::vec3(0, 0, 1);
            }
            else if (mode == PERSPECTIVE_MODE) {
                ray.origin = glm::vec3(0, 0, -5);
                ray.direction = glm::vec3(ui, vj, 0) - ray.origin;
            }

            // Find first intersecting sphere
            float distance_to_closest_sphere = FLT_MAX;
            glm::vec3 color_of_closest_sphere;
            for (Sphere s : world) {
                float curr_dist = intersection(ray, s);
                if (curr_dist < distance_to_closest_sphere) {
                    distance_to_closest_sphere = curr_dist;
                    color_of_closest_sphere = s.color;
                }
            }

            // Set color of pixel to sphere color
            // if a sphere was found
            if (distance_to_closest_sphere < FLT_MAX) {
                float r = color_of_closest_sphere.x * 255;
                float g = color_of_closest_sphere.y * 255;
                float b = color_of_closest_sphere.z * 255;
                image.set_pixel(i, j, make_colour(r, g, b));
            }

            // Set color of pixel to background color if a sphere wasn't found
            else {
                image.set_pixel(i, j, make_colour(BG_R, BG_G, BG_B));
            }
        }
    }

    if (mode == ORTHOGRAPHGIC_MODE)
        image.save_image("orthographic-ray-traced.bmp");
    else if (mode == PERSPECTIVE_MODE)
        image.save_image("perspective-ray-traced.bmp");
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(IMAGE_WIDTH, IMAGE_HEIGHT);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
        Sphere(glm::vec3(0, 1, 3), 2, glm::vec3(0,0,1)),
        Sphere(glm::vec3(4, 0, 2), 2, glm::vec3(1,1,1))
    };

    // render the world
    render(ORTHOGRAPHGIC_MODE, image, world);
    render(PERSPECTIVE_MODE, image, world);

    std::cout << "Success" << std::endl;
}


