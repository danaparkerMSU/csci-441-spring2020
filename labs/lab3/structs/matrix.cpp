#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>

#include "matrix.h"

Matrix operator+(const Matrix &m1, const Matrix &m2) {
    if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
        std::cout 
            << "\noperator*():"
            << "\nYour matrix dimensions aren't the same."
            << "\nMatrix 1: " << m1.size() << " x " << m1[0].size()
            << "\nMatrix 2: " << m2.size() << " x " << m2[0].size()
            << "\nExiting program..."
            << std::endl;
        std::exit(1);
    }

    Matrix rv(m1.size(), std::vector<float>(m1[0].size(), 0));
    for (int i = 0; i < m1.size(); i++) {
        for (int j = 0; j < m1[0].size(); j++) {
            rv[i][j] = m1[i][j] + m2[i][j];
        } 
    }

    return rv;
}

Matrix operator*(const Matrix &m1, const Matrix &m2) {
    
    // Aliases to make this easier
    const int& m1_rows = m1.size();
    const int& m1_cols = m1[0].size();
    const int& m2_rows = m2.size();
    const int& m2_cols = m2[0].size();

    // If the two matrices dimensions are incompatible,
    // throw an error and exit program.
    if (m1_cols != m2_rows) {
        std::cout
            << "\noperator*():"
            << "\nNumber of columns in the first matrix (" << m1_cols << ") "
            << "does not equal the number of rows in the second matrix ("
            << m2_rows << ")."
            << "\nThe two input matrices are incompatible."
            << "\nExiting program..."
            << std::endl;
        std::exit(1);
    }
    
    // The matrix to be returned
    Matrix rv(m1_rows, std::vector<float>(m2_cols, 0));

    // Multiply the two input matrices
    // Help from: 
    // https://www.mathwarehouse.com/algebra/matrix/multiply-matrix.php
    for (int i = 0; i < m1_rows; i++) {
        for (int j = 0; j < m2_cols; j++) {
            for (int k = 0; k < m1_cols; k++) {
                rv[i][j] += m1[i][k] * m2[k][j];
            }
        }
    }

    return rv;
}

std::vector<float> operator*(
        const std::vector<float> m1,
        const Matrix &m2) {
    return operator*(m2, m1);
}

std::vector<float> operator*(
        const Matrix &m1,
        const std::vector<float> m2) {
    
    // Aliases to make this easier
    const int& m1_rows = m1.size();
    const int& m1_cols = m1[0].size();
    const int& m2_rows = m2.size();
    const int& m2_cols = 1;

    if (m1_cols != m2_rows) {
        std::cout
            << "\noperator*():"
            << "\nNumber of columns in the first matrix (" << m1_cols << ") "
            << "does not equal the number of rows in the second matrix ("
            << m2_rows << ")."
            << "\nThe two input matrices are incompatible."
            << "\nExiting program..."
            << std::endl;
        std::exit(1);
    }
    
    // The vector to be returned
    std::vector<float> rv(m1_rows, 0); 
    float temp = 0.0;

    // Multiply the two input matrices
    // Help from: 
    // https://www.mathwarehouse.com/algebra/matrix/multiply-matrix.php
    for (int i = 0; i < m1_rows; i++) {
        for (int j = 0; j < m1_cols; j++) {
            temp += m1[i][j] * m2[j];
        }
        rv[i] = temp;
        temp = 0.0;
    }

    return rv;
}

Matrix arr1d_to_matrix(const float in_arr[], const int len_arr, const int point_length) {
    Matrix rv(len_arr / point_length, std::vector<float>(point_length, 0));

    int arr_idx = 0;
    for (int i = 0; i < len_arr / point_length; i++) {
        for (int j = 0; j < point_length; j++) {
            rv[i][j] = in_arr[arr_idx];
            arr_idx++;
        }
    }

    return rv;
}

void print_matrix(const std::vector<float> vec) {
    for (int i = 0; i < vec.size(); i++) {
        std::cout << vec[i] << " ";
    }
    std::cout << std::endl;
}

void print_matrix(const Matrix &m) {
    for (int i = 0; i < m.size(); i++) {
        for (int j = 0; j < m[0].size(); j++) {
            std::cout << m[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

