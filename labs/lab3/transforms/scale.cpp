#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>
#include <vector>

#include "scale.h"

Matrix create_scale_matrix(
        const float scale_by_x,
        const float scale_by_y) {
    Matrix rv(3, std::vector<float>(3, 0));

	rv[0][0] = scale_by_x;
	rv[0][1] = 0;
	rv[0][2] = 0;
	rv[1][0] = 0;
	rv[1][1] = scale_by_y;
	rv[1][2] = 0;
	rv[2][1] = 0;
	rv[2][1] = 0;
	rv[2][2] = 1;

	return rv;
}
