#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>
#include <vector>

#include "translate.h"

Matrix create_translation_matrix(
        const float translate_by_x,
        const float translate_by_y) {
    Matrix rv(3, std::vector<float>(3, 0));

    for (int i = 0; i < rv.size(); i++) {
        for (int j = 0; j < rv.size(); j++) {
            if (i == j) {
                rv[i][j] = 1;
            }
            else if (i == 0 && j == rv.size() - 1) {
                rv[i][j] = translate_by_x;
            }
            else if (i == 1 && j == rv.size() - 1) {
                rv[i][j] = translate_by_y;
            }
        } 
    }

    return rv;
}

