// Class idea inspired by https://www.3dgep.com/understanding-the-view-matrix/

#ifndef MATRIX4
#define MATRIX4

#include <iostream>
#include <cmath>

#define TRANSLATION 1
#define SCALE       2
#define ROTATE_X    3
#define ROTATE_Y    4
#define ROTATE_Z    5
#define PERSPECTIVE 6
#define ORTHOGONAL  7

#define VEC_X       0
#define VEC_Y       1
#define VEC_Z       2

class Vec4 {

    public:
        float vec[4];

        // Default constructor
        Vec4() {
            vec[0] = 0;
            vec[1] = 0;
            vec[2] = 0;
            vec[3] = 0;
        }

        // Construtor that takes in x, y, z, and w values as input
        Vec4(float in0, float in1, float in2, float in3) {
            vec[0] = in0;
            vec[1] = in1;
            vec[2] = in2;
            vec[3] = in3;
        }

        float& operator[] (int i) {
            return vec[i];
        }

        const float& operator[] (int i) const {
            return vec[i];
        }

        void print() {
            std::cout << vec[0] << " "
                      << vec[1] << " "
                      << vec[2] << " "
                      << vec[3]
                      << std::endl;
        }
};

class Matrix4 {

    public:
        Vec4 cols[4];

        // Default constructor (identity matrix)
        Matrix4() {
            cols[0] = Vec4(1, 0, 0, 0);
            cols[1] = Vec4(0, 1, 0, 0);
            cols[2] = Vec4(0, 0, 1, 0);
            cols[3] = Vec4(0, 0, 0, 1);
        }

        // Constructor that takes in four Vec4s as input
        Matrix4(Vec4 v1, Vec4 v2, Vec4 v3, Vec4 v4) {
            cols[0] = v1;
            cols[1] = v2;
            cols[2] = v3;
            cols[3] = v4;
        }

        // Scale or Translation matrix constructor
        Matrix4(
            const int mode,
            const float x,
            const float y,
            const float z)
        {
            switch(mode) {
                case(TRANSLATION): {
                    cols[0] = Vec4(1, 0, 0, x);
                    cols[1] = Vec4(0, 1, 0, y);
                    cols[2] = Vec4(0, 0, 1, z);
                    cols[3] = Vec4(0, 0, 0, 1);
                    break;
                }
                case(SCALE): {
                    cols[0] = Vec4(x, 0, 0, 0);
                    cols[1] = Vec4(0, y, 0, 0);
                    cols[2] = Vec4(0, 0, z, 0);
                    cols[3] = Vec4(0, 0, 0, 1);
                    break;
                }
                default: {
                    std::cout << "\n\nInvalid mode (" << mode << ")"
                              << "\nPlease enter either TRANSLATION or SCALE"
                              << std::endl;
                    exit(1);
                }
            }
        }

        // Rotation matrix constructor
        Matrix4(const int mode, float degrees) {
            if (mode == ROTATE_X) {
                cols[0] = Vec4(1, 0, 0, 0);
                cols[1] = Vec4(0, cos(degrees), -sin(degrees), 0);
                cols[2] = Vec4(0, sin(degrees), cos(degrees), 0);
                cols[3] = Vec4(0, 0, 0, 1);
            }
            else if (mode == ROTATE_Y) {
                cols[0] = Vec4(cos(degrees), 0, sin(degrees), 0);
                cols[1] = Vec4(0, 1, 0, 0);
                cols[2] = Vec4(-sin(degrees), 0, cos(degrees), 0);
                cols[3] = Vec4(0, 0, 0, 1);
            }
            else if (mode == ROTATE_Z) {
                cols[0] = Vec4(cos(degrees), -sin(degrees), 0, 0);
                cols[1] = Vec4(sin(degrees),  cos(degrees), 0, 0);
                cols[2] = Vec4(0, 0, 1, 0);
                cols[3] = Vec4(0, 0, 0, 1);
            }
            else {
                std::cout << "\n\nInvalid mode (" << mode << ")"
                            << "\nPlease enter either ROTATE_X, ROTATE_Y, or ROTATE_Z"
                            << std::endl;
                exit(1);
            }
        }

        // Perspective and orthogonal constructor
        Matrix4(int mode) {
            const float l =  -1.0; // left
            const float r =   1.0; // right
            const float b =  -1.0; // bottom
            const float t =   1.0; // top
            const float n =  -1.0; // near
            const float f =   1.0; // far

            if (mode == ORTHOGONAL) {
                cols[0] = Vec4(2/(r-l), 0, 0, 0);
                cols[1] = Vec4(0, 2/(t-b), 0, 0);
                cols[2] = Vec4(0, 0, 2/(n-f), 0);
                cols[3] = Vec4(
                    -(r+l) / (r-l),
                    -(t+b) / (t-b),
                    -(f+n) / (n-f),
                    1.0
                );
            }
            else if (mode == PERSPECTIVE) {
                cols[0] = Vec4(n, 0,  0,   0);
                cols[1] = Vec4(0, n,  0,   0);
                cols[2] = Vec4(0, 0,  f+n, 1);
                cols[3] = Vec4(0, 0, -n*f, 0);
            }
            else {
                std::cout << "\n\nInvalid mode (" << mode << ")"
                            << "\nPlease enter either PERSPECTIVE or ORTHOGONAL"
                            << std::endl;
                exit(1);
            }
        }

        Vec4& operator[] (int i) {
            return cols[i];
        }

        const Vec4& operator[] (int i) const {
            return cols[i];
        }

        void print() {
            cols[0].print();
            cols[1].print();
            cols[2].print();
            cols[3].print();
        }
};

Vec4 normalize(const Vec4 vec) {
    float magnitude = sqrt(
        pow(vec[0], 2)
        + pow(vec[1], 2)
        + pow(vec[2], 2)
    );

    if (magnitude == 0) {
        return Vec4(0, 0, 0, 0);
    }

    Vec4 rv;
    
    rv[0] = vec[0] / magnitude;
    rv[1] = vec[1] / magnitude;
    rv[2] = vec[2] / magnitude;

    return rv;
}

Vec4 cross_product(Vec4 vec1, Vec4 vec2) {
    return Vec4(
        (vec1[1] * vec2[2]) - (vec1[2] * vec2[1]),
        (vec1[2] * vec2[0]) - (vec1[0] * vec2[2]),
        (vec1[0] * vec2[1]) - (vec1[1] * vec2[0]),
        0
    );
}

// Matrix4 * Vec4
Vec4 operator*(const Matrix4 mat, const Vec4 vec) {
    return Vec4(
        mat[0][0] * vec[0] + mat[1][0] * vec[1] + mat[2][0] * vec[2] + mat[3][0] * vec[3],
        mat[0][1] * vec[0] + mat[1][1] * vec[1] + mat[2][1] * vec[2] + mat[3][1] * vec[3],
        mat[0][2] * vec[0] + mat[1][2] * vec[1] + mat[2][2] * vec[2] + mat[3][2] * vec[3],
        mat[0][3] * vec[0] + mat[1][3] * vec[1] + mat[2][3] * vec[2] + mat[3][3] * vec[3]
    );
}

// Vec4 * Matrix4
Vec4 operator*(const Vec4 vec, const Matrix4 mat) {
    return Vec4(
        vec[0] * mat[0][0] + vec[1] * mat[0][1] + vec[2] * mat[0][2] + vec[3] * mat[0][3],
        vec[0] * mat[1][0] + vec[1] * mat[1][1] + vec[2] * mat[1][2] + vec[3] * mat[1][3],
        vec[0] * mat[2][0] + vec[1] * mat[2][1] + vec[2] * mat[2][2] + vec[3] * mat[2][3],
        vec[0] * mat[3][0] + vec[1] * mat[3][1] + vec[2] * mat[3][2] + vec[3] * mat[3][3]
    );
}

// Matrix4 * Matrix4
Matrix4 operator*(const Matrix4 one, const Matrix4 two)
{
    Vec4 X(
        one[0][0] * two[0][0] + one[1][0] * two[0][1] + one[2][0] * two[0][2] + one[3][0] * two[0][3],
        one[0][1] * two[0][0] + one[1][1] * two[0][1] + one[2][1] * two[0][2] + one[3][1] * two[0][3],
        one[0][2] * two[0][0] + one[1][2] * two[0][1] + one[2][2] * two[0][2] + one[3][2] * two[0][3],
        one[0][3] * two[0][0] + one[1][3] * two[0][1] + one[2][3] * two[0][2] + one[3][3] * two[0][3]
    );
    Vec4 Y(
        one[0][0] * two[1][0] + one[1][0] * two[1][1] + one[2][0] * two[1][2] + one[3][0] * two[1][3],
        one[0][1] * two[1][0] + one[1][1] * two[1][1] + one[2][1] * two[1][2] + one[3][1] * two[1][3],
        one[0][2] * two[1][0] + one[1][2] * two[1][1] + one[2][2] * two[1][2] + one[3][2] * two[1][3],
        one[0][3] * two[1][0] + one[1][3] * two[1][1] + one[2][3] * two[1][2] + one[3][3] * two[1][3]
    );
    Vec4 Z(
        one[0][0] * two[2][0] + one[1][0] * two[2][1] + one[2][0] * two[2][2] + one[3][0] * two[2][3],
        one[0][1] * two[2][0] + one[1][1] * two[2][1] + one[2][1] * two[2][2] + one[3][1] * two[2][3],
        one[0][2] * two[2][0] + one[1][2] * two[2][1] + one[2][2] * two[2][2] + one[3][2] * two[2][3],
        one[0][3] * two[2][0] + one[1][3] * two[2][1] + one[2][3] * two[2][2] + one[3][3] * two[2][3]
    );
    Vec4 W(
        one[0][0] * two[3][0] + one[1][0] * two[3][1] + one[2][0] * two[3][2] + one[3][0] * two[3][3],
        one[0][1] * two[3][0] + one[1][1] * two[3][1] + one[2][1] * two[3][2] + one[3][1] * two[3][3],
        one[0][2] * two[3][0] + one[1][2] * two[3][1] + one[2][2] * two[3][2] + one[3][2] * two[3][3],
        one[0][3] * two[3][0] + one[1][3] * two[3][1] + one[2][3] * two[3][2] + one[3][3] * two[3][3]
    );

    return Matrix4(X, Y, Z, W);
}

Matrix4 init_camera(Vec4 eye_pos, Vec4 target, Vec4 up_dir) {

    // Compute forward vector
    Vec4 forward_dir(
        eye_pos[0] - target[0],
        eye_pos[1] - target[1],
        eye_pos[2] - target[2],
        eye_pos[3] - target[3]
    );
    forward_dir = normalize(forward_dir);

    // Compute left vector
    Vec4 left_dir = cross_product(forward_dir, up_dir);
    normalize(left_dir);

    // Compute orthonormal up vector
    Vec4 orth_norm_up = cross_product(forward_dir, left_dir);

    float trans_x = -(left_dir[VEC_X]) * eye_pos[VEC_X]
                    - left_dir[VEC_Y]  * eye_pos[VEC_Y]
                    - left_dir[VEC_Z]  * eye_pos[VEC_Z];
    
    float trans_y = -(up_dir[VEC_X]) * eye_pos[VEC_X]
                    - up_dir[VEC_Y]  * eye_pos[VEC_Y]
                    - up_dir[VEC_Z]  * eye_pos[VEC_Z];
    
    float trans_z = -(forward_dir[VEC_X]) * eye_pos[VEC_X]
                    - forward_dir[VEC_Y]  * eye_pos[VEC_Y]
                    - forward_dir[VEC_Z]  * eye_pos[VEC_Z];

    Matrix4 rv(
        Vec4(left_dir[VEC_X], orth_norm_up[VEC_X], forward_dir[VEC_X], 0),
        Vec4(left_dir[VEC_Y], orth_norm_up[VEC_Y], forward_dir[VEC_Y], 0),
        Vec4(left_dir[VEC_Z], orth_norm_up[VEC_Z], forward_dir[VEC_Z], 0),
        Vec4(trans_x,         trans_y,             trans_z,            1)
    );

    return rv;
}

#endif
