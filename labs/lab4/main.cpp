#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>

#include "matrix/Matrix4.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

const float input_delta = 0.001;

bool backslash_is_pressed = false;
int current_mode = ORTHOGONAL;

float R_x_delta = 90;
float R_y_delta = 90;
float R_z_delta = 90;
float S_delta = 1.0;
float T_x_delta = 0.0;
float T_y_delta = 0.0;
float T_z_delta = 0.0;

int C_delta = -20.0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    
    // =========================================
    //
    // Camera
    //
    // =========================================

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        if (C_delta < 20) {
            C_delta += input_delta;
        }   
    }
    
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        if (C_delta > -20) {
            C_delta -= input_delta;
        }
    }

    // ==========================================
    //
    // Translation
    // 
    // ==========================================

    // Move model up
    // via Translation matrix's y-component
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        T_y_delta += input_delta;
    }

    // Move model down
    // via Translation matrix's y-component
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        T_y_delta -= input_delta;
    }

    // Move model left
    // via Translation matrix's x-component
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        T_x_delta -= input_delta;
    }

    // Move model right
    // via Translation matrix's x-component
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        T_x_delta += input_delta;
    }

    // Move model forward
    // via Translation matrix's z-component
    if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS) {
        T_z_delta += input_delta;
    }

    // Move model backward
    // via Translation matrix's z-component
    if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS) {
        T_z_delta -= input_delta;
    }

    // ==========================================
    //
    // Scale
    //
    // ==========================================

    // Scale model down
    // via Scale matrix's x-, y-, and z-components
    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        S_delta *= 0.99;
    }

    // Scale model up
    // via Scale matrix's x-, y-, and z-components
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        S_delta *= 1.01;
    }

    // Rotate counterclockwise about x-axis
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
        R_x_delta--;
    }

    // Rotate clockwise about x-axis
    if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
        R_x_delta++;
    }

    // Rotate counterclockwise about y-axis
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
        R_y_delta--;
    }

    // Rotate clockwise about y-axis
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
        R_y_delta++;
    }

    // Rotate counterclockwise about z-axis
    if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS) {
        R_z_delta--;
    }

    // Rotate countclockwise about z-axis
    if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS) {
        R_z_delta++;
    }

    // ==========================================
    //
    //  Projection
    //
    // ==========================================

    // Switch between projection types
    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS) {
        backslash_is_pressed = true;
    }

    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_RELEASE) {
        if (current_mode == ORTHOGONAL) {
            current_mode = PERSPECTIVE;
        }
        else if (current_mode == PERSPECTIVE) {
            current_mode = ORTHOGONAL;
        }
        backslash_is_pressed = false;
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };
    
    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Part 1: View Matrix
        Matrix4 M_C(
            Vec4(1, 0, 0, 0),
            Vec4(0, 1, 0, C_delta),
            Vec4(0, 0, 1, 20),
            Vec4(0, 0, 0, 1)
        );
        
        // Part 2: Model Matrix
        Matrix4 T(TRANSLATION, T_x_delta, T_y_delta, T_z_delta);
        Matrix4 S(SCALE, S_delta, S_delta, S_delta);
        Matrix4 R_x(ROTATE_X, R_x_delta);
        Matrix4 R_y(ROTATE_Y, R_y_delta);
        Matrix4 R_z(ROTATE_Z, R_z_delta);
        Matrix4 R = R_z * R_y * R_x;

        // Part 3: Projection
        Matrix4 O(ORTHOGONAL);
        Matrix4 P(PERSPECTIVE);

        // Put Parts 1, 2, and 3 together
        // into a transformation matrix
        Matrix4 proj_M;

        if (current_mode == ORTHOGONAL) {
            proj_M = Matrix4(
                Vec4(1, 0, 0, 0),
                Vec4(0, 1, 0, 0),
                Vec4(0, 0, 1, 0),
                Vec4(0, 0, 0, 1)
            );
        }
        else if (current_mode == PERSPECTIVE) {
            proj_M = O * P * M_C;
        }

        Matrix4 model_M = T * R * S;

        int count = 0;
        float proj_M_as_arr[16];
        float model_M_as_arr[16];

        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                proj_M_as_arr[count] = proj_M[i][j];
                model_M_as_arr[count] = model_M[i][j];
                count++;
            }
        }

        std::cout << "\nR_z:" << std::endl;
        R_z.print();

        // activate shader
        shader.use();

        int model_location = glGetUniformLocation(shader.id(), "model_M");
        int proj_location = glGetUniformLocation(shader.id(), "proj_M");
        glUniform1fv(proj_location, 16, proj_M_as_arr);
        glUniform1fv(model_location, 16, model_M_as_arr);

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
