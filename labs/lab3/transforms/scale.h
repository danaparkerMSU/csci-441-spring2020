#ifndef SCALE_H
#define SCALE_H

#include <vector>
#include "../structs/matrix.h"

Matrix create_scale_matrix(const float, const float);

#endif
