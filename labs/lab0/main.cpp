#include <iostream>
#include <string>

// Part 5
class Vector3 {
public:
    float x;
    float y;
    float z;

    // Default constructor
    Vector3() : x(0), y(0), z(0) {

    }

    // Non-default constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // std::cout << "in Vector3 constructor" << std::endl;
    }

    // Destructor
    ~Vector3() {
        // std::cout << "in Vector3 destructor" << std::endl;
    }
};

// Part 5
Vector3 add(const Vector3& v, Vector3 v2) {
    return Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
}

// Part 6
Vector3 operator+(const Vector3& v, const Vector3& v2) {
    return(Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z));
}

// Part 6
std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector

    stream << "[" << v.x << ", "
                  << v.y << ", "
                  << v.z << "]";

    return stream;
}

int main(int argc, char** argv) {

    // Part 4a
    std::cout << "\nPart 4a" << std::endl;
    for(int i = 0; i < argc; i++) {
        if(i != argc - 1) {
            std::cout << argv[i] << " ";
        }
        else {
            std::cout << argv[i] << std::endl;
        }
    }

    // Part 4b
    std::cout << "\nPart 4b" << std::endl;
    std::cout << "Give me a name for Part4b of this lab:"
              << std::endl;

    std::string name;
    std::cin >> name;
    std::cout << "hello " << name << std::endl;

    // Part 5
    std::cout << "\nPart 5" << std::endl;
    Vector3 a(1, 2, 3);
    Vector3 b(4, 5, 6);
    Vector3 c = add(a, b);
    std::cout << "c.x = " << c.x << std::endl;
    std::cout << "c.y = " << c.y << std::endl;
    std::cout << "c.z = " << c.z << std::endl;

    // Part 6
    std::cout << "\nPart 6" << std::endl;
    Vector3 v(1,2,3);
    Vector3 v2(4,5,6);
    std::cout << v+v2 << std::endl;

    // Part 7
    std::cout << "\nPart 7" << std::endl;
    Vector3 part7a(0, 0, 0);
    part7a.y = 5;
    std::cout << part7a << std::endl;
    
    Vector3* part7b = new Vector3(0, 0, 0);
    std::cout << *part7b << std::endl;
    delete part7b;

    // Part 8
    std::cout << "\nPart 8" << std::endl;
    Vector3  part8a[10];
    Vector3* part8b = new Vector3[10];

    for (int i = 0; i < 10; i++) {
        part8b[i].y = 5;
        std::cout << "part8b[" << i << "] = " << part8b[i] << std::endl;
    }

    return 0;
}