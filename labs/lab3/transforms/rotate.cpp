#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>
#include <vector>
#include <cmath>

#include "rotate.h"

Matrix create_rotation_matrix(const float degrees) {
    Matrix rv(3, std::vector<float>(3, 0));

    rv[0][0] =  cos(degrees);
    rv[0][1] = -sin(degrees);
    rv[0][2] =  0;
    rv[1][0] =  sin(degrees);
    rv[1][1] =  cos(degrees);
    rv[1][2] =  0;
    rv[2][1] =  0;
    rv[2][1] =  0;
    rv[2][2] =  1;

    return rv;
}

