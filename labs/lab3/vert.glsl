#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

out vec3 myColor;

uniform mat3 t_mat;

void main() {
    float new_x = (t_mat[0][0] * aPos.x) + (t_mat[0][1] * aPos.y) + t_mat[0][2];
    float new_y = (t_mat[1][0] * aPos.x) + (t_mat[1][1] * aPos.y) + t_mat[1][2];
    gl_Position = vec4(new_x, new_y, 0, 1.0);
    myColor = aColor;
}

