#ifndef TRANSLATE_H
#define TRANSLATE_H

#include <vector>
#include "../structs/matrix.h"

Matrix create_translation_matrix(const float, const float);
Matrix translate(const Matrix, const float, const float);

#endif
