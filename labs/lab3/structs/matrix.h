#ifndef MATRIX_H
#define MATRIX_H

#include <vector>

// Credit to Troy Oster for showing me this link
// for how to to do this:
// https://stackoverflow.com/questions/8767166/passing-a-2d-array-to-a-c-function
typedef std::vector<std::vector<float>> Matrix;

Matrix              arr1d_to_matrix(const float[], const int, const int);
Matrix              operator+(const Matrix &, const Matrix &);

Matrix              operator*(const Matrix &, const Matrix &);
std::vector<float>  operator*(const Matrix &, const std::vector<float>);
std::vector<float>  operator*(const std::vector<float>, const Matrix &);

void                matrix_to_arr(const Matrix, float (&)[9]);
void                print_matrix(const std::vector<float>);
void                print_matrix(const Matrix &);

#endif
