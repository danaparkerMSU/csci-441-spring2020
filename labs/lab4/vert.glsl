#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

uniform float model_M[16];
uniform float proj_M[16];

out vec3 ourColor;

void main() {
    float x = (model_M[0] * aPos.x) + (model_M[1] * aPos.y) + (model_M[2] * aPos.z) + model_M[3];
    float y = (model_M[4] * aPos.x) + (model_M[5] * aPos.y) + (model_M[6] * aPos.z) + model_M[7];
    float z = (model_M[8] * aPos.x) + (model_M[9] * aPos.y) + (model_M[10] * aPos.z) + model_M[11];
    float w = (model_M[12] * aPos.x) + (model_M[13] * aPos.y) + (model_M[14] * aPos.z) + model_M[15];

    x = (proj_M[0] * x) + (proj_M[1] * y) + (proj_M[2] * z) + (proj_M[3] * w);
    y = (proj_M[4] * x) + (proj_M[5] * y) + (proj_M[6] * z) + (proj_M[7] * w);
    z = (proj_M[8] * x) + (proj_M[9] * y) + (proj_M[10] * z) + (proj_M[11] * w);
    w = (proj_M[12] * x) + (proj_M[13] * y) + (proj_M[14] * z) + (proj_M[15] * w);

    gl_Position = vec4(x, y, -z, w);
    ourColor = aColor;
}
