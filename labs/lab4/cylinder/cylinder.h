#ifndef CYLINDER
#define CYLINDER

#define PI 3.14159

#include <vector>
#include <cmath>

class Cylinder {
    public:
        std::vector<float> cylinder;

        Cylinder() {
            float theta = 0;
            float angle = 0;

            for (int i = 0; i < 16; i++) {

                angle = theta + (PI / 8);

                //first triangle (upper left)
                cylinder[i++] = cos(theta);
                cylinder[i++] = 1.0f;
                cylinder[i++] = sin(theta);
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;

                cylinder[i++] = cos(theta);
                cylinder[i++] = -1.0f;
                cylinder[i++] = sin(theta);
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;

                cylinder[i++] = cos(phi);
                cylinder[i++] = 1.0f;
                cylinder[i++] = sin(phi);
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;

                //second triangle (lower right)
                cylinder[i++] = cos(theta);
                cylinder[i++] = -1.0f;
                cylinder[i++] = sin(theta);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0.0f;

                cylinder[i++] = cos(phi);
                cylinder[i++] = -1.0f;
                cylinder[i++] = sin(phi);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0.0f;

                cylinder[i++] = cos(phi);
                cylinder[i++] = 1.0f;
                cylinder[i++] = sin(phi);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0.0f;

                //top triangle
                cylinder[i++] = cos(theta);
                cylinder[i++] = 1.0f;
                cylinder[i++] = sin(theta);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;

                cylinder[i++] = cos(phi);
                cylinder[i++] = 1.0f;
                cylinder[i++] = sin(phi);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;

                cylinder[i++] = 0;
                cylinder[i++] = 1.0f;
                cylinder[i++] = 0;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;

                //bottom triangle
                cylinder[i++] = cos(theta);
                cylinder[i++] = -1.0f;
                cylinder[i++] = sin(theta);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;

                cylinder[i++] = cos(phi);
                cylinder[i++] = -1.0f;
                cylinder[i++] = sin(phi);
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;

                cylinder[i++] = 0;
                cylinder[i++] = -1.0f;
                cylinder[i++] = 0;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 0.0f;
                cylinder[i++] = 1.0f;

                theta = phi;
            }
        }
};

#endif
