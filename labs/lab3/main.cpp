#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>
#include <vector>

#include "transforms/translate.h"
#include "transforms/rotate.h"
#include "transforms/scale.h"

#include "structs/matrix.h"

#define STATIC_MODE            0
#define ROTATE_CENTER_MODE     1 
#define ROTATE_OFF_CENTER_MODE 2
#define SCALE_MODE             3
#define IMPRESS_ME_MODE        4
        
int mode = STATIC_MODE;
bool spacebar_is_pressed = false;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }

    int spacebar = glfwGetKey(window, GLFW_KEY_SPACE);
    
    if (spacebar == GLFW_PRESS) {
        spacebar_is_pressed = true;
    }

    if (spacebar == GLFW_RELEASE && spacebar_is_pressed) {
        mode++;

        if (mode > IMPRESS_ME_MODE) {
            mode = STATIC_MODE;
        }
        spacebar_is_pressed = false;
    }
}

int main(void) {

    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */
        Matrix R;
        Matrix S;
        Matrix T;
        Matrix m;

        switch(mode) {
            case STATIC_MODE: {
                m = {
                    {1, 0, 0},
                    {0, 1, 0},
                    {0, 0, 1}
                };
                break;
            }
            case ROTATE_CENTER_MODE: {
                m = create_rotation_matrix(glfwGetTime());
                break; 
            }
            case ROTATE_OFF_CENTER_MODE: {
                R = create_rotation_matrix(glfwGetTime());
                m = create_translation_matrix(0.75, 0.25);
                m = R * m;
                break;
            }
            case SCALE_MODE: {
                float scale_by = cos(glfwGetTime());
                m = create_scale_matrix(scale_by, scale_by);
                break;
            }
            case IMPRESS_ME_MODE: {
                float s = sin(glfwGetTime());
                float c = cos(glfwGetTime());
                T = create_translation_matrix(s, c);
                R = create_rotation_matrix(s);
                S = create_scale_matrix(c, s);
                m = S * R * T * T * R * S * S * R * R * T * T;
                break;
            }
            default: {
                std::cout << "Invalid render mode.\nExiting program...";
                std::exit(1);
            }
        }

        float in_mat[9] = {
            m[0][0], m[0][1], m[0][2],
            m[1][0], m[1][1], m[1][2],
            m[2][0], m[2][1], m[2][2]
        };

        int t_location = glGetUniformLocation(shader.id(), "t_mat");
        glUniformMatrix3fv(t_location, 1, GL_FALSE, &in_mat[0]);

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
